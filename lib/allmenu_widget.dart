import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AllMenuWidget extends StatefulWidget {
  const AllMenuWidget({Key? key}) : super(key: key);

  @override
  _AllMenuWidgetState createState() => _AllMenuWidgetState();
}

class _AllMenuWidgetState extends State<AllMenuWidget> {
  List<String> foods = [
    'ข้าวขาหมู',
    'ข้าวหมูแดง',
    'ข้าวมันไก่',
    'ข้าวคลุกกะปิ',
    'ข้าวผัด',
    'โจ๊ก',
    'ราดหน้า',
    'ผัดไท',
    'เย็นตาโฟ',
    'สุกี้',
    'ผัดซีอิ๊ว',
    'สปาเก็ตตี้',
    'ขนมปังสังขยา',
    'เค้กหน้านิ่ม',
    'ทาร์ตไข่',
    'บราวนี่',
    'ขนมชั้น',
    'ข้าวต้มมัด',
    'มะม่วง',
    'ส้ม',
    'แตงโม',
    'มังคุด',
    'เงาะ',
    'ทุเรียน',
    'นม',
    'ชา',
    'กาแฟ',
    'น้ำผลไม้',
    'น้ำอัดลม',
    'โกโก้'
  ];
  TextEditingController foodController = TextEditingController();
  TextEditingController foodrandom = TextEditingController();
  var foodnew = '';
  @override
  void dispose() {
    foodController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.5), BlendMode.dstATop),
              image: AssetImage('assets/asian-food.jpg'),
              fit: BoxFit.cover),
        ),
        child: Center(
            child: Column(
          children: [
            Container(
                child: Text('สุ่มอาหารทั้งหมด',
                    style: GoogleFonts.prompt(
                        fontSize: 24, fontWeight: FontWeight.w600))),
            Container(
              child: bodyContent(),
            ),
            Container(
              // ignore: deprecated_member_use
              child: RaisedButton(
                onPressed: () {
                  var random = Random();
                  foodnew = foods[random.nextInt(foods.length)];
                  foodrandom.text = foodnew;
                  print(foodnew);
                },
                child: Text('กดเพื่อสุ่ม',
                    style: GoogleFonts.prompt(color: Colors.white)),
                color: Colors.orangeAccent[700],
              ),
            )
          ],
        )),
      ),
      //backgroundColor: Colors.amber[200],
    );
  }

  bodyContent() {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: 400,
        height: 300,
        child: TextField(
          controller: foodrandom,
          readOnly: true,
          enableInteractiveSelection: false,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: '',
            labelStyle: GoogleFonts.prompt(),
          ),
        ),
        padding: EdgeInsets.all(30),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.purple[100],
            borderRadius: BorderRadius.circular(20)),
      ),
    );
  }
}
