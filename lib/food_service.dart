import 'package:randomfood/food.dart';

var lastId = 5;
var mockFoods = [
  Food(id: 1, name: 'ข้าวขาหมู', type: 'ทั้งหมด'),
  Food(id: 2, name: 'ข้าวคลุกกะปิ', type: 'ทั้งหมด'),
  Food(id: 3, name: 'ข้าวซอย', type: 'ทั้งหมด'),
  Food(id: 4, name: 'ข้าวมันไก่', type: 'ทั้งหมด')
];

int getNewId() {
  return lastId;
}

Future<void> addNew(Food food) {
  return Future.delayed(Duration(seconds: 1), () {
    mockFoods.add(Food(id: getNewId(), name: food.name, type: 'ทั้งหมด'));
  });
}

Future<void> delFoods(Food food) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockFoods.indexWhere((item) => food.id == food.id);
    mockFoods.removeAt(index);
  });
}

Future<List<Food>> getFoods() {
  return Future.delayed(Duration(seconds: 1), () => mockFoods);
}
