import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Knowledge extends StatefulWidget {
  const Knowledge({Key? key}) : super(key: key);

  @override
  _KnowledgeState createState() => _KnowledgeState();
}

class _KnowledgeState extends State<Knowledge> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ความรู้', style: GoogleFonts.prompt()),
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          scrollDirection: Axis.vertical,
          children: [
            Padding(padding: EdgeInsets.all(8)),
            Text(
              ' 5 อาหารขึ้นชื่อประจำชาติ\n ที่อาจมีต้นกำเนิดจากประเทศอื่น',
              style: GoogleFonts.prompt(fontSize: 24),
            ),
            Padding(padding: EdgeInsets.all(8)),
            Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '  ครัวซองต์ ขนมฝรั่งเศสที่มีต้นกำเนิดในออสเตรีย',
                  style: GoogleFonts.prompt(fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(0xFFEEEEEE),
                      ),
                      child: Image.asset(
                        'assets/croissants.jpg',
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      'พอเอ่ยถึงขนมชนิดนี้หลายคนนึกถึงประเทศฝรั่งเศส \nอย่างแน่นอน เพราะชื่อของมันเป็นภาษาฝรั่งเศสขนาดนี้\nแต่แท้จริงแล้ว ครัวซองต์มีต้นกำเนิดที่กรุงเวียนนา\nประเทศออสเตรีย',
                      style: GoogleFonts.prompt(fontSize: 16),
                    ),
                  ],
                ),
                Text(
                  '   แฮมเบอร์เกอร์ สัญลักษณ์ของอเมริกาที่มาจากเยอรมนี',
                  style: GoogleFonts.prompt(fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'ในช่วงทศวรรษที่ 18 ชาวยุโรปรวมทั้งชาวเยอรมัน\nได้อพยพไปยังสหรัฐและได้นำเมนูนี้ไปเผยแพร่ด้วย\nโดยเรียกว่า แฮมเบอร์เกอร์สเต๊ก',
                      style: GoogleFonts.prompt(fontSize: 16),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(0xFFEEEEEE),
                      ),
                      child: Image.asset(
                        'assets/hamburger.jpg',
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    )
                  ],
                ),
                Text(
                  '   กิมจิ ที่ว่าเป็นเกาหลีแน่ ๆ แต่รับมาจากจีน',
                  style: GoogleFonts.prompt(fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(0xFFEEEEEE),
                      ),
                      child: Image.asset(
                        'assets/kimchi.jpg',
                        width: 200,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      'ทั่วโลกรู้จักกิมจิผ่านวัฒนธรรมบันเทิงของเกาหลี\nที่ได้รับความนิยมเพิ่มมากขึ้นเรื่อย ๆ\nแต่หากย้อนประวัติศาสตร์กลับไป\nกิมจินั้นมีต้นกำเนิดในประเทศจีน',
                      style: GoogleFonts.prompt(fontSize: 16),
                    ),
                  ],
                ),
                Text(
                  '   ชาอังกฤษ เดินทางมาจากจีนและอินเดีย',
                  style: GoogleFonts.prompt(fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'แท้จริงแล้ว "ชาอังกฤษ" กลับไม่ใช่พืชที่มีต้นกำเนิด\nในประเทศอังกฤษ มีเพียงวัฒนธรรมการดื่มชา\nแบบอังกฤษเท่านั้นที่จะเรียกว่าเป็นของอังกฤษ',
                      style: GoogleFonts.prompt(fontSize: 16),
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(0xFFEEEEEE),
                      ),
                      child: Image.asset(
                        'assets/tea.jpg',
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  ],
                ),
                Text(
                  '   ซาชิมิ ปลาดิบญี่ปุ่นได้รับอิทธิพลจากแถบแม่น้ำโขง',
                  style: GoogleFonts.prompt(fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(0xFFEEEEEE),
                      ),
                      child: Image.asset(
                        'assets/DSC.jpg',
                        width: 200,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      'ในบทความของ South China Morning Post\nบอกวไว้ว่าวัฒนธรรมการรับประธานอาหารดิบ\nเริ่มต้นที่แถบแม่น้ำโขงในเอเชียตะวันออกเฉียงใต้\nก่อนจะเข้าสู้ประเทศจีนและญี่ปุ่นได้นำเอาวัฒนธรรมนี้\nเข้ามาในยุคนาระ',
                      style: GoogleFonts.prompt(fontSize: 16),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
      backgroundColor: Colors.amber[200],
    );
  }
}
