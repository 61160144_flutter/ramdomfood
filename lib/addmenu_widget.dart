import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AddMeuWidget extends StatefulWidget {
  const AddMeuWidget({Key? key}) : super(key: key);

  @override
  _AddMeuWidgetState createState() => _AddMeuWidgetState();
}

class _AddMeuWidgetState extends State<AddMeuWidget> {
  List<String> foods = [];
  TextEditingController foodController = TextEditingController();
  TextEditingController foodrandom = TextEditingController();
  var foodnew = '';
  @override
  void dispose() {
    foodController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่มเมนูของตัวเอง'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              padding: EdgeInsets.all(10),
              child: Text(
                ' เพิ่มเมนูที่ชอบได้ตรงนี้เลย',
                style: GoogleFonts.prompt(
                    fontSize: 24,
                    color: Colors.black,
                    fontWeight: FontWeight.w600),
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                    margin: EdgeInsets.all(20),
                    child: TextField(
                      controller: foodController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'พิมพ์ที่นี่เพื่อเพิ่มเมนูอาหาร',
                        labelStyle: GoogleFonts.prompt(),
                      ),
                    )),
                Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: MaterialButton(
                      onPressed: () {
                        addToList();
                        print(foods.toString());
                        clearText();
                      },
                      child: Text('เพิ่ม',
                          style: GoogleFonts.prompt(color: Colors.white)),
                      color: Colors.orangeAccent[700],
                    ))
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            width: 400,
            height: 300,
            child: TextField(
              controller: foodrandom,
              readOnly: true,
              enableInteractiveSelection: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: '',
                labelStyle: GoogleFonts.prompt(),
              ),
            ),
            padding: EdgeInsets.all(30),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.purple[100],
                borderRadius: BorderRadius.circular(20)),
          ),
          Container(
            // ignore: deprecated_member_use
            child: RaisedButton(
              onPressed: () {
                var random = Random();
                foodnew = foods[random.nextInt(foods.length)];
                foodrandom.text = foodnew;
                print(foodnew);
              },
              child: Text('กดเพื่อสุ่ม',
                  style: GoogleFonts.prompt(color: Colors.white)),
              color: Colors.orangeAccent[700],
            ),
          )
        ],
      ),
      backgroundColor: Colors.amber[200],
    );
  }

  void addToList() {
    if (foodController.text.isNotEmpty) {
      setState(() {
        foods.add(foodController.text);
      });
    }
  }

  void clearText() {
    foodController.clear();
  }
}
