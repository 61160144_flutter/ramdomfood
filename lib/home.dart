import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:randomfood/bakerrymenu_widget.dart';
import 'package:randomfood/drinkmenu_widget.dart';
import 'package:randomfood/fruitmenu_widget.dart';
import 'package:randomfood/noodlemenu_widget.dart';
import 'package:randomfood/ricemenu_widget.dart';

import 'allmenu_widget.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Food Random'),
        ),
        backgroundColor: Color(0xFFFFF599),
        body: SafeArea(
          child: Column(mainAxisSize: MainAxisSize.max, children: [
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  ' ยินดีต้อนรับ',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.prompt(
                      fontSize: 24,
                      color: Color(0xFFF66331),
                      fontWeight: FontWeight.w600),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/5284.jpg',
                      height: 200,
                      width: 400,
                      fit: BoxFit.fitWidth,
                    )
                  ],
                ),
                Text('  หมวดหมู่สุ่มอาหาร',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.prompt(
                        fontSize: 24,
                        color: Color(0xFFF66331),
                        fontWeight: FontWeight.w300)),
                Padding(padding: EdgeInsets.all(10)),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AllMenuWidget()));
                                },
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                  ),
                                  child: Image.asset(
                                    'assets/3908937.png',
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                )),
                            Text(
                              'สุ่มทุกเมนู',
                              style: GoogleFonts.prompt(fontSize: 16),
                            ),
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BakerryMenu()));
                                },
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                  ),
                                  child: Image.asset(
                                    'assets/3508688.png',
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                )),
                            Text(
                              'สุ่มของหวาน',
                              style: GoogleFonts.prompt(fontSize: 16),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => RiceMenu()));
                                },
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                  ),
                                  child: Image.asset(
                                    'assets/3508690.png',
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )),
                            Text(
                              'สุ่มเมนูข้าว',
                              style: GoogleFonts.prompt(fontSize: 16),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FruitMenu()));
                              },
                              child: Container(
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFFEEEEEE),
                                ),
                                child: Image.asset(
                                  'assets/3508687.png',
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Text(
                              'สุ่มผลไม้',
                              style: GoogleFonts.prompt(fontSize: 16),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => NoodleMenu()));
                                },
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                  ),
                                  child: Image.asset(
                                    'assets/3508689.png',
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.cover,
                                  ),
                                )),
                            Text(
                              'สุ่มเมนูเส้น',
                              style: GoogleFonts.prompt(fontSize: 16),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DrinkMenu()));
                              },
                              child: Container(
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFFEEEEEE),
                                ),
                                child: Image.asset(
                                  'assets/3524968.png',
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            Text(
                              'สุ่มเครื่องดื่ม',
                              style: GoogleFonts.prompt(fontSize: 16),
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ]),
        ));
  }
}
