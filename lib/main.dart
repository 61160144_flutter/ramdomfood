//import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:randomfood/addmenu_widget.dart';
import 'package:randomfood/home.dart';
import 'package:randomfood/know_widget.dart';

void main() {
  runApp(MaterialApp(title: 'Food Random', home: YourApplication()));
}

class YourApplication extends StatefulWidget {
  @override
  YourApplicationState createState() {
    return new YourApplicationState();
  }
}

class YourApplicationState extends State<YourApplication> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getBody(index),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: (value) => setState(() => index = value),
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            // ignore: deprecated_member_use
            title: Text(
              "Home",
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add_box,
            ),
            // ignore: deprecated_member_use
            title: Text(
              "Add",
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.help,
            ),
            // ignore: deprecated_member_use
            title: Text(
              "Knowledge",
            ),
          ),
        ],
        type: BottomNavigationBarType.fixed,
      ),
    );
  }

  Widget _getBody(int index) {
    switch (index) {
      case 0:
        return Home();
      case 1:
        return AddMeuWidget();
      case 2:
        return Knowledge();
    }

    return Center(
      child: Text("There is no page builder for this index."),
    );
  }
}
