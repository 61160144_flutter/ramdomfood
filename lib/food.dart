class Food {
  int id;
  String name;
  String type;

  Food({
    required this.id,
    required this.name,
    required this.type,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'type': type,
    };
  }

  List<Food> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Food(
          id: maps[i]['id'], name: maps[i]['name'], type: maps[i]['type']);
    });
  }

  @override
  String toString() {
    return 'Food{id: $id, name: $name, type: $type}';
  }
}
